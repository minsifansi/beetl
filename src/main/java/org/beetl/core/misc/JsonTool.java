package org.beetl.core.misc;

public interface JsonTool {
    public String render(Object o);
}
